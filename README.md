<p align="center">
    <img src="https://codeberg.org/PrivacyBlog/Balance/raw/branch/main/fastlane/metadata/android/de-DE/images/icon.png" title="Balance Logo">
</p>

# Balance App

Jeden Tag eine neue buddhistische Weisheit mit Hintergrundbildern von Unsplash.com

## Minimalistisch

* Keine Werbung
* Keine In-App-Käufe
* Keine Tracker
* Free & Open-Source nach GPLv3-Lizenz
* Benötigt keine Berechtigungen oder Internetzugriff
* Built from scratch

## Screenshots

<img src="https://codeberg.org/PrivacyBlog/Balance/raw/branch/main/fastlane/metadata/android/de-DE/images/phoneScreenshots/1.png" width=350 title="Balance App">


## Author

[PrivacyBlog](https://codeberg.org/PrivacyBlog/)


## Lizenzen

### App:

Frei nach [GNU GPLv3-Lizenz](https://codeberg.org/PrivacyBlog/Balance/src/branch/main/LICENSE).

### In-App Icon-/Bild-Lizenzen:

* Frei zur Verfügung gestellte Bilder von Unsplash.com