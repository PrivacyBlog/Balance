package org.eukalyptus.balance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.Random;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_main);
        setupGui();
    }

    @Override
    protected void onResume(){
        super.onResume();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_main);
        setupGui();
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_main);
        setupGui();
    }

    public String getStringResourceByName(String msgIDname) {
        String packageName = getPackageName();
        int resId = getResources().getIdentifier(msgIDname, "string", packageName);
        return getResources().getString(resId);
    }


    public void setupGui(){

        // Deklaration
        int picResID;
        int PicNxtInt;
        int TxtNxtInt;
        String TxtNxtString;

        // Erstelle Anwendungseinstellungen
        SharedPreferences sharedPreferences = getSharedPreferences("org.eukalyptus.balance", Context.MODE_PRIVATE);
        // Hole gespeicherten Wert zum Bildschlüssel
        int storedDateDayInt = sharedPreferences.getInt("DAY_KEY", 0);


        // Hole aktuelles Datum
        Date actualDate = new Date();
        // Hole Tag aus dem Datum und konvertiere zu String
        String actualDateDayString = (String) DateFormat.format("dd", actualDate);
        // Parse String zu Int
        int actualDateDayInt = Integer.parseInt(actualDateDayString);


        // Wenn heutiges Datum und gespeichertes Datum übereinstimmen
        if(actualDateDayInt == storedDateDayInt){
            // dann behalte das Bild bei
            picResID = sharedPreferences.getInt("PICTURE_KEY",0);

        }else{

            // SONST
            // NEUES BILD
            final TypedArray imgs = getResources().obtainTypedArray(R.array.arrayBackgroundImages);

            PicNxtInt = sharedPreferences.getInt("NEXTPICTURE", 0);

            if(PicNxtInt >= imgs.length()){
                PicNxtInt = 0;
            }

            picResID = imgs.getResourceId(PicNxtInt, 0);

            PicNxtInt = PicNxtInt+1;

            // NEUER TEXT
            String msgCounterString = getResources().getString(R.string.messageCounter);
            final int msgCounter = Integer.parseInt(msgCounterString);
            TxtNxtString = sharedPreferences.getString("MSG_KEY","0");

            TxtNxtInt = Integer.parseInt(TxtNxtString);
            TxtNxtInt = TxtNxtInt+1;

            if(TxtNxtInt > msgCounter){
                TxtNxtInt = 1;
            }

            TxtNxtString = String.valueOf(TxtNxtInt);

            // und speichere in den Settings das Datum, dazugehörige Bild & Text
            sharedPreferences.edit().putInt("PICTURE_KEY", picResID).apply();
            sharedPreferences.edit().putInt("NEXTPICTURE", PicNxtInt).apply();
            sharedPreferences.edit().putInt("DAY_KEY",actualDateDayInt).apply();
            sharedPreferences.edit().putString("MSG_KEY",TxtNxtString).apply();
        }


        ImageView backgroundImageView = (ImageView) findViewById(R.id.dailyMessageImageView);
        backgroundImageView.setImageResource(picResID);

        // Hol die msgID aus den Appeinstellungen
        String msgID = sharedPreferences.getString("MSG_KEY","1");

        TextView textView = (TextView) findViewById(R.id.dailyMessage);

        String messageString = "message" + msgID;
        String message = getStringResourceByName(messageString);
        textView.setText(message);
    }
}
